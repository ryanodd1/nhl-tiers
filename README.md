## Fun NHL Stats

This site is a collection of NHL standings/stats I find interesting.

- PPG Standings: Teams are listed by Points Per Game, and how historically often their PPG makes the playoffs.
- More coming

Made with React/Redux. The NHL stats API is used to gather current standings data.
