export default [
  {
    id: 6,
    name: 'Boston Bruins',
    city: 'Boston',
    slug: 'bos'
  },
  {
    id: 14,
    name: 'Tampa Bay Lightning',
    city: 'Tampa Bay',
    slug: 'tbl'
  },
  {
    id: 10,
    name: 'Toronto Maple Leafs',
    city: 'Toronto',
    slug: 'tor'
  },
  {
    id: 13,
    name: 'Florida Panthers',
    city: 'Florida',
    slug: 'fla'
  },
  {
    id: 7,
    name: 'Buffalo Sabres',
    city: 'Buffalo',
    slug: 'buf'
  },
  {
    id: 8,
    name: 'Montreal Canadiens',
    city: 'Montreal',
    slug: 'mtl'
  },
  {
    id: 9,
    name: 'Ottawa Senators',
    city: 'Ottawa',
    slug: 'ott'
  },
  {
    id: 17,
    name: 'Detroit Red Wings',
    city: 'Detroit',
    slug: 'det'
  },

  {
    id: 1,
    name: 'New Jersey Devils',
    city: 'New Jersey',
    slug: 'njd'
  },
  {
    id: 15,
    name: 'Washington Capitals',
    city: 'Washington',
    slug: 'was'
  },
  {
    id: 5,
    name: 'Pittsburgh Penguins',
    city: 'Pittsburgh',
    slug: 'pit'
  },
  {
    id: 2,
    name: 'New York Islanders',
    city: 'N.Y. Islanders',
    slug: 'nyi'
  },
  {
    id: 12,
    name: 'Carolina Hurricanes',
    city: 'Carolina',
    slug: 'car'
  },
  {
    id: 29,
    name: 'Columbus Blue Jackets',
    city: 'Columbus',
    slug: 'cbj'
  },
  {
    id: 3,
    name: 'New York Rangers',
    city: 'N.Y. Rangers',
    slug: 'nyr'
  },
  {
    id: 4,
    name: 'Philadelphia Flyers',
    city: 'Philadelphia',
    slug: 'phi'
  },

  {
    id: 19,
    name: 'St. Louis Blues',
    city: 'St. Louis',
    slug: 'stl'
  },
  {
    id: 25,
    name: 'Dallas Stars',
    city: 'Dallas',
    slug: 'dal'
  },
  {
    id: 52,
    name: 'Winnipeg Jets',
    city: 'Winnipeg',
    slug: 'wpg'
  },
  {
    id: 21,
    name: 'Colorado Avalanche',
    city: 'Colorado',
    slug: 'col'
  },
  {
    id: 18,
    name: 'Nashville Predators',
    city: 'Nashville',
    slug: 'nas'
  },
  {
    id: 30,
    name: 'Minnesota Wild',
    city: 'Minnesota',
    slug: 'min'
  },
  {
    id: 16,
    name: 'Chicago Blackhawks',
    city: 'Chicago',
    slug: 'chi'
  },

  {
    id: 54,
    name: 'Vegas Golden Knights',
    city: 'Vegas',
    slug: 'vgk'
  },
  {
    id: 53,
    name: 'Arizona Coyotes',
    city: 'Arizona',
    slug: 'ari'
  },
  {
    id: 22,
    name: 'Edmonton Oilers',
    city: 'Edmonton',
    slug: 'edm'
  },
  {
    id: 20,
    name: 'Calgary Flames',
    city: 'Calgary',
    slug: 'cgy'
  },
  {
    id: 23,
    name: 'Vancouver Canucks',
    city: 'Vancouver',
    slug: 'van'
  },
  {
    id: 28,
    name: 'San Jose Sharks',
    city: 'San Jose',
    slug: 'sjs'
  },
  {
    id: 26,
    name: 'Los Angeles Kings',
    city: 'Los Angeles',
    slug: 'lak'
  },
  {
    id: 24,
    name: 'Anaheim Ducks',
    city: 'Anaheim',
    slug: 'ana'
  },
  // Old Teams
  {
    id: 27,
    name: 'Phoenix Coyotes',
    city: 'Phoenix',
    slug: 'phx'
  },
  {
    id: 11,
    name: 'Atlanta Thrashers',
    city: 'Atlanta',
    slug: 'atl'
  }
]
