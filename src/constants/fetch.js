export const STANDINGS_ENDPOINT = `https://statsapi.web.nhl.com/api/v1/standings/byLeague`

export const FIRST_HISTORIC_YEAR = 2005 // First year with 3-point system
export const LAST_HISTORIC_YEAR = 2019