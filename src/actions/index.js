import axios from 'axios';
import * as CONST from '../constants/fetch'

export function loadCurrentStandings(){
  return function(dispatch) {
    // Dispatching REQUEST action, which tells our app that we are requesting standings.
    dispatch({
      type: 'LOAD_CURRENT_STANDINGS_REQUEST'
    });
    return axios.get(`${CONST.STANDINGS_ENDPOINT}?season=${CONST.LAST_HISTORIC_YEAR + 1}${CONST.LAST_HISTORIC_YEAR + 2}`)
    .then(response => {
      if (!response || response.status !== 200) {
        // If request was failed, dispatching FAILURE action.
        dispatch({
          type: 'LOAD_CURRENT_STANDINGS_FAILURE',
          error: response.error
        });
      } else {
        // When everything is ok, dispatching SUCCESS action.
        dispatch({
          type: 'LOAD_CURRENT_STANDINGS_SUCCESS',
          standings: response.data
        });
      }
    });
  }
}

export function loadHistoricStandings(){
  return function(dispatch) {
    dispatch({
      type: 'LOAD_HISTORIC_STANDINGS_REQUEST'
    });
    for(let year = CONST.FIRST_HISTORIC_YEAR; year <= CONST.LAST_HISTORIC_YEAR; year++) {
      axios.get(`${CONST.STANDINGS_ENDPOINT}?season=${year}${year+1}`)
      .then(response => {
        if (!response || response.status !== 200) {
          // If request was failed, dispatching FAILURE action.
          dispatch({
            type: 'LOAD_HISTORIC_STANDINGS_FAILURE',
            error: response.error
          });
        } else {
          dispatch({
            type: 'LOAD_HISTORIC_STANDINGS_ADD',
            standings: response.data
          });
        }
      });
    }
  }
}

export function createPPGClinchData(clinched, missed) {
  return { type: 'CREATE_PPG_CLINCH_DATA', clinched, missed }
}