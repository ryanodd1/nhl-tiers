import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import styles from './styles/StandingsCell.module.css';

export default class StandingsCell extends Component {
  render(){
    return (
      <Card className={styles.cell} >
        <img src={require('./assets/teamLogos/' + this.props.team.slug + '.svg')} className={styles.image} alt="logo" />
        <span>{this.props.team.ppg !== -1 ? this.props.team.ppg : '-'} PPG</span>
        <span>{this.props.team.clinchOdds !== -1 ? this.props.team.clinchOdds : '- '}%</span>
      </Card>
    );
  }
}
