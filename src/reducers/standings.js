import * as CONST from '../constants/fetch'
const NUM_HISTORIC_YEARS = (CONST.LAST_HISTORIC_YEAR - CONST.FIRST_HISTORIC_YEAR) + 1

const INITIAL_STATE = {
  loadedCurrent: false,
  loadedHistoric: false,
  current: null,
  historic: [],
  clinchedPPGs: [],
  missedPPGs: [],
  error: null,
}

const standings = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'LOAD_CURRENT_STANDINGS_REQUEST':
      return Object.assign({}, state, {
        current: null,
        loadedCurrent: false,
      })
    case 'LOAD_CURRENT_STANDINGS_SUCCESS':
      return Object.assign({}, state, {
        current: action.standings,
        loadedCurrent: true
      })
    case 'LOAD_CURRENT_STANDINGS_FAILURE':
      return Object.assign({}, state, {
        error: action.error
      })
    case 'LOAD_HISTORIC_STANDINGS_REQUEST':
      return Object.assign({}, state, {
        historic: [],
        loadedHistoric: false
      })
    case 'LOAD_HISTORIC_STANDINGS_ADD':
      state.historic.push(action.standings)
      return Object.assign({}, state, {
        loadedHistoric: state.historic.length === NUM_HISTORIC_YEARS
      })
    case 'LOAD_HISTORIC_STANDINGS_FAILURE':
      return Object.assign({}, state, {
        error: action.error
      })
    case 'CREATE_PPG_CLINCH_DATA':
      return Object.assign({}, state, {
        clinchedPPGs: action.clinched,
        missedPPGs: action.missed
      })
    default:
      return state
  }
}
export default standings