import React, { Component } from 'react'

import { connect } from 'react-redux'
import { loadCurrentStandings, loadHistoricStandings, createPPGClinchData } from './actions'

import StandingsCell from './StandingsCell'
import Card from '@material-ui/core/Card';

import * as standingsParser from './helpers/parsers/standingsParser'
import * as standingsAlgos from './helpers/algos/standingsAlgos'

import styles from './styles/Standings.module.css'

class Standings extends Component {
  
  componentDidMount() {
    this.props.dispatch(loadCurrentStandings())
    this.props.dispatch(loadHistoricStandings())
  }

  // When loading is finished, additional state setup is needed
  componentDidUpdate(prevProps, prevState) {
    if (this.props.loaded && !prevProps.loaded) {
      let result = standingsParser.getPPGByClinch(this.props.historicStandings)
      this.props.dispatch(createPPGClinchData(result.clinched, result.missed))
    }
  }

  renderHeader(text, tooltip=''){
    return <div key={text} className={styles.header}> {text} </div>
  }

  renderGroups() {
    let groups = []
    let teams = standingsParser.getTeamsByPPG(this.props.currentStandings)
    teams.forEach(team => {
      team['clinchOdds'] = standingsAlgos.clinchOddsByPPG(team.ppg, this.props.clinchedPPGs, this.props.missedPPGs)
    })
    groups.push(this.renderHeader('Worthy'))
    groups.push(this.renderCells(teams.filter(team => team.clinchOdds > 99)))
    groups.push(this.renderHeader('Neither'))
    groups.push(this.renderCells(teams.filter(team => (team.clinchOdds > 0 && team.clinchOdds < 100) || team.clinchOdds === -1)))
    groups.push(this.renderHeader('Unworthy'))
    groups.push(this.renderCells(teams.filter(team => team.clinchOdds < 1 && team.clinchOdds >= 0)))
    return groups
  }

  renderCells(teams) {
    let cells = []
    teams.forEach(team => {
      team['clinchOdds'] = standingsAlgos.clinchOddsByPPG(team.ppg, this.props.clinchedPPGs, this.props.missedPPGs)
      cells.push(
        <StandingsCell
          key={team.id}
          team={team}
        />
      )
    })
    return cells
  } 

  render(){
  return (
    <div className={styles.page}>
      <div className={styles.pageContent}>
        <Card>
          <div className={styles.title}>Which teams are having playoff-worthy seasons?</div>
          <div className={styles.subtitle}>Teams are listed by Points Per Game, and how historically often their PPG makes the playoffs.</div>
        </Card>
        <div className={styles.list}>
          {this.props.ready ? this.renderGroups() : 'Loading!'}
        </div>
        <div className={styles.footer}>{this.props.ready ? this.props.currentStandings.copyright : '' }</div>
      </div>
    </div>
  )
  }
}

function mapStateToProps(state) {
  return {
    //loaded = fetched api data, ready = prepped api data. Don't display until ready
    loaded: state.standings.loadedCurrent && state.standings.loadedHistoric,
    ready: state.standings.clinchedPPGs.length && state.standings.missedPPGs.length,
    currentStandings: state.standings.current,
    historicStandings: state.standings.historic,
    clinchedPPGs: state.standings.clinchedPPGs,
    missedPPGs: state.standings.missedPPGs
  }
}

export default connect(mapStateToProps)(Standings)
