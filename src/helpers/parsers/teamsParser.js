import teams from '../../constants/teams'

function getTeamByID(id){
  return teams.find(team => id === team.id)
}

export default { getTeamByID }
