import teamsParser from './teamsParser'

export function getTeamsByPPG(data){
  let teams = []

  data.records[0].teamRecords.forEach(record => {
    let ppg = (record.points / record.gamesPlayed).toFixed(3);

    let team = teamsParser.getTeamByID(record.team.id)
    team['gp'] = record.gamesPlayed;
    team['ppg'] = isNaN(ppg) ? -1 : ppg;
    teams.push(team)
  })


  // sort by gp descending
  teams.sort((a, b) => {return b.gp - a.gp})
  // sort by ppg descending
  teams.sort((a, b) => {return b.ppg - a.ppg})
  return teams
}

export function getPPGByClinch(data){
  let clinched = []
  let missed = []

  data.forEach(year => {
    year.records[0].teamRecords.forEach(record => {
      let ppg = (record.points / record.gamesPlayed).toFixed(4)
      let didClinch = record.clinchIndicator
      if (didClinch)
        clinched.push(ppg)
      else
        missed.push(ppg)
    })
  })

  return {
    clinched: clinched.sort(),
    missed: missed.sort()
  }
}
