
import { sortedIndex, sortedLastIndex } from 'lodash'

// Takes in a ppg val, and sorted asc arrays: historical clinched ppgs and missed ppgs
// Returns 0-100 (2 decimals) historical chances of clinching
export function clinchOddsByPPG(ppg, clinched, missed) {
  let clinchesAtOrBelow = sortedLastIndex(clinched, ppg)
  if (ppg === -1) return -1;
  let missesAtOrAbove = missed.length - sortedIndex(missed, ppg)
  return ((clinchesAtOrBelow / (clinchesAtOrBelow + missesAtOrAbove)) * 100).toFixed(0)
}
